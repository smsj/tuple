# Tuple

A simple, low-level matrix <--> mqtt "bridge".

## Usage

`go get gitlab.com/smsj/tuple`

`tuple -homeserver http://localhost:8008 -username testuser -password t123e123s -broker tcp://localhost:1883`

The rooms have to joined manually at this point via another client.

Configuration using environment vars is also possible:
- `USERNAME`
- `PASSWORD`
- `HOMESERVER`
- `BROKER`

The command line args overwrite these.


## Matrix -> MQTT

The bridge will send all Matrix Message Events to:

Topic: `test`

Content: _the received Matrix event_, like 
```json
{
  "type": "m.room.message",
  "sender": "@tuple:eiselecloud.de",
  "content": {
    "body": "foo",
    "msgtype": "m.text"
  },
  "origin_server_ts": 1590951283344,
  "unsigned": {
    "age": 165
  },
  "event_id": "$yNhWPC-6zi85SQGpkqsbZv6K_aTW8yrpkgn0y83FgiI",
  "room_id": "!jxyndfUCCLyUOLeZuI:eiselecloud.de"
}
```

## MQTT -> Matrix

Topic: `test`

Content: _Any Matrix Message Event Content_, like
```json
{
    "msgtype":"m.text",
    "body": "foo"
}
```

The sender will be the `testuser` user.

The `testuser` should receive its own Matrix events and publish them to MQTT.


## Design

This Bridge should closely follow the Matrix Client Server Specs, with useful exceptions.

The `eventType` is included in the mqtt topic, so that IOT devices can choose to subscribe only to compatible 
(proprietary) events

This is my first golang project, so don't judge to hard on programming decisions. 

## Useful links
* [Matrix Client Server Api Specs](https://matrix.org/docs/spec/client_server/r0.6.1)
